package com.riskisyendi.daycareapps.controller;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.riskisyendi.daycareapps.R;
import com.riskisyendi.daycareapps.common.Animation;
import com.riskisyendi.daycareapps.common.CircleGraphicsUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class AccountActivity extends AppCompatActivity {

    private EditText mEdtEmail, mEdtPass;
    private Button mBtnLogin;
    private ViewFlipper mFlpLoginSignUp;
    private TextView mBtnRegister, mBtnBackLogin;
    private Animation mAnimLoginSignUp;
    private ImageView mImgPicUserName;
    private CircleGraphicsUtil mCircleGraphicsUtil;
    private Bitmap mBitmapPicUserName;
    String ImageProfile;
    String imageEncoded;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        pageAccount();

    }

    public void pageAccount(){

        mEdtEmail = (EditText) findViewById(R.id.edtEmail);
        mEdtPass = (EditText) findViewById(R.id.edtPass);
        mBtnLogin = (Button) findViewById(R.id.btnLogin);
        mBtnRegister = (TextView) findViewById(R.id.btnRegister);
        mBtnBackLogin = (TextView) findViewById(R.id.btnBackLogin);
        mFlpLoginSignUp = (ViewFlipper) findViewById(R.id.flpLoginSignUp);
        mAnimLoginSignUp = new Animation(mFlpLoginSignUp);

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(AccountActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAnimLoginSignUp.nextLoginSignUp();
            }
        });

        mBtnBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAnimLoginSignUp.prevLoginSignUp();
            }
        });

//        cameraControl();

    }

    public void cameraControl() {
        // retrieve a reference to the UI button
        mImgPicUserName = (ImageView) findViewById(R.id.imgPicUserName);
        // handle button clicks
        /**
         * Click method to handle user pressing button to launch camera
         */
        mImgPicUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogImage();


            }
        });
    }

    private void dialogImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        TextView title = new TextView(this);
        title.setText("Add Photo");
        title.setTextColor(ContextCompat.getColor(this, R.color.textBlack));
        title.setBackgroundColor(ContextCompat.getColor(this, R.color.pastelYellow));
        title.setPadding(20, 20, 20, 20);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(22);


        AlertDialog.Builder builder = new AlertDialog.Builder(
                AccountActivity.this);



        builder.setCustomTitle(title);

        // builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    //                if (v.getId() == R.id.imgPicUserName) {
                    try {
                        // use standard intent to capture an image
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        /*create instance of File with name img.jpg*/
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        /*put uri as extra in intent object*/
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent, 1);
                    } catch (ActivityNotFoundException anfe) {
                        // display an error message
                        String errorMessage = "Whoops - your device doesn't support capturing images!";
                        Toast toast = Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent();
                    // call android default gallery
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.putExtra("crop", "true");
                    // ******** code for crop image
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    // indicate output X and Y
                    intent.putExtra("outputX", 256);
                    intent.putExtra("outputY", 256);

                    try {
                        intent.putExtra("return-data", true);
                        startActivityForResult(Intent.createChooser(intent,"Complete action using"),3);

                    }catch (Exception E){
                        E.printStackTrace();
                    }
                }else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Handle user returning from both capturing and cropping the image
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // user is returning from capturing an image using the camera
            if (requestCode == 1) {
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                //Crop the captured image using an other intent
                try {
                /*the user's device may not support cropping*/
                    performCrop(Uri.fromFile(file));
                } catch (ActivityNotFoundException aNFE) {
                    //display an error message if user device doesn't support
                    String errorMessage = "Sorry - your device doesn't support the crop action!";
                    Toast toast = Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
            if (requestCode == 2) {
                //Create an instance of bundle and get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                mBitmapPicUserName = extras.getParcelable("data");
                // retrieve a reference to the ImageView
                mImgPicUserName = (ImageView) findViewById(R.id.imgPicUserName);
                // display the returned cropped image

                // picView.setImageBitmap(graphicUtil.getRoundedShape(thePic,(float)1.5,92));
                mImgPicUserName.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(mBitmapPicUserName, 16));

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                mBitmapPicUserName.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] b = byteArrayOutputStream.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
/*---------------------Image ini nanti dikirim ke webservice---------------------------*/
//                spGetUser.createImage(imageEncoded);
            }
//            if (requestCode == 3) {
//                Bundle extras2 = data.getExtras();
//                if (extras2 != null) {
//                    Bitmap photo = extras2.getParcelable("data");
//                    mImgPicUserName.setImageBitmap(mCircleGraphicsUtil.getCircleBitmap(photo, 16));
//                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                    photo.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//                    byte[] b = byteArrayOutputStream.toByteArray();
//                    imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
///*---------------------Image ini nanti dikirim ke webservice---------------------------*/
////                    spGetUser.createImage(imageEncoded);
//
//                }
            }
        }
//    }
    /**
     * Helper method to carry out crop operation
     */
    public void performCrop(Uri picUri) {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 2);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }


}
