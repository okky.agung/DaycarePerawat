package com.riskisyendi.daycareapps.rowItem;

/**
 * Created by riskisyendi on 10/17/16.
 */

public class ChecklistChildRowItem {

//    private String child_list;
    private String strNo;
    private String strDesc;

    public ChecklistChildRowItem(String strNo, String strDesc) {
        this.strNo = strNo;
        this.strDesc = strDesc;
    }

    public String getStrNo() {
        return strNo;
    }

    public void setStrNo(String strNo) {
        this.strNo = strNo;
    }

    public String getStrDesc() {
        return strDesc;
    }

    public void setStrDesc(String strDesc) {
        this.strDesc = strDesc;
    }


    //    public ChecklistChildRowItem(String child_list) {
//        this.child_list = child_list;
//    }
//
//    public String getName() {
//        return child_list;
//    }
//
//    public void setName(String Name) {
//        this.child_list = Name;
//    }


}
