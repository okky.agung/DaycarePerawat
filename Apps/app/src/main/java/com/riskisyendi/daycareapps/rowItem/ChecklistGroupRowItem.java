package com.riskisyendi.daycareapps.rowItem;

import java.util.ArrayList;

/**
 * Created by riskisyendi on 10/17/16.
 */

public class ChecklistGroupRowItem {

    private String strHeaderName;
    private ArrayList<ChecklistChildRowItem> Items;

    public ChecklistGroupRowItem(String strHeaderName) {
        this.strHeaderName = strHeaderName;
    }

    public ChecklistGroupRowItem(String strHeaderName, ArrayList<ChecklistChildRowItem> items) {
        this.strHeaderName = strHeaderName;
        Items = items;
    }

    public String getStrHeaderName() {
        return strHeaderName;
    }

    public void setStrHeaderName(String strHeaderName) {
        this.strHeaderName = strHeaderName;
    }

    public ArrayList<ChecklistChildRowItem> getItems() {
        return Items;
    }

    public void setItems(ArrayList<ChecklistChildRowItem> items) {
        Items = items;
    }
}


//public class ChecklistGroupRowItem {
//
//    private String group_list;
//    private ArrayList<ChecklistChildRowItem> Items;
//
//    public ChecklistGroupRowItem(String group_list, ArrayList<ChecklistChildRowItem> items) {
//        this.group_list = group_list;
//        Items = items;
//    }
//
//    public String getGroup_list() {
//        return group_list;
//    }
//
//    public void setGroup_list(String group_list) {
//        this.group_list = group_list;
//    }
//
//    public ArrayList<ChecklistChildRowItem> getItems() {
//        return Items;
//    }
//    public void setItems(ArrayList<ChecklistChildRowItem> Items) {
//        this.Items = Items;
//    }
//
//
//}

//
//
//    public ChecklistGroupRowItem(String strHeaderName, String strHeaderDate) {
//        this.strHeaderName = strHeaderName;
//        this.strHeaderDate = strHeaderDate;
//    }
//
//    public ChecklistGroupRowItem(String strHeaderName, String strHeaderDate, ArrayList<ChecklistChildRowItem> items) {
//        this.strHeaderName = strHeaderName;
//        this.strHeaderDate = strHeaderDate;
//        Items = items;
//    }
//
//    public String getStrHeaderName() {
//        return strHeaderName;
//    }
//
//    public void setStrHeaderName(String strHeaderName) {
//        this.strHeaderName = strHeaderName;
//    }
//
//    public String getStrHeaderDate() {
//        return strHeaderDate;
//    }
//
//    public void setStrHeaderDate(String strHeaderDate) {
//        this.strHeaderDate = strHeaderDate;
//    }
//
//    public ArrayList<ChecklistChildRowItem> getItems() {
//        return Items;
//    }
//
//    public void setItems(ArrayList<ChecklistChildRowItem> items) {
//        Items = items;
//    }