package com.riskisyendi.daycareapps.common;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ViewFlipper;

/**
 * Created by riskisyendi on 3/7/17.
 */

public class Animation {

    private ViewFlipper mFlpLoginSignUp;

    public Animation(ViewFlipper mFlpLoginSignUp) {
        this.mFlpLoginSignUp = mFlpLoginSignUp;
    }


    public  Animation nextLoginSignUp (){
        mFlpLoginSignUp.setInAnimation(inFromRightAnimation());
        mFlpLoginSignUp.setOutAnimation(outToLeftAnimation());
        mFlpLoginSignUp.showNext();
        return this;
    }

    public  Animation prevLoginSignUp (){
        mFlpLoginSignUp.setInAnimation(inFromLeftAnimation());
        mFlpLoginSignUp.setOutAnimation(outToRightAnimation());
        mFlpLoginSignUp.showPrevious();
        return this;
    }

    public android.view.animation.Animation inFromRightAnimation() {

        android.view.animation.Animation inFromRight = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  +1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;

    }
    private android.view.animation.Animation outToLeftAnimation() {

        android.view.animation.Animation outtoLeft = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;

    }

    private android.view.animation.Animation inFromLeftAnimation() {

        android.view.animation.Animation inFromLeft = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;

    }
    private android.view.animation.Animation outToRightAnimation() {

        android.view.animation.Animation outtoRight = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  +1.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;

    }

    public android.view.animation.Animation inFromBottomAnimation() {

        android.view.animation.Animation inFromBottom = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  +1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromBottom.setDuration(500);
        inFromBottom.setInterpolator(new AccelerateInterpolator());
        return inFromBottom;

    }
    private android.view.animation.Animation outToTopAnimation() {

        android.view.animation.Animation outtoTop = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f
        );
        outtoTop.setDuration(500);
        outtoTop.setInterpolator(new AccelerateInterpolator());
        return outtoTop;

    }

    private android.view.animation.Animation inFromTopAnimation() {

        android.view.animation.Animation inFromTop = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  -1.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromTop.setDuration(500);
        inFromTop.setInterpolator(new AccelerateInterpolator());
        return inFromTop;

    }
    private android.view.animation.Animation outToBottomAnimation() {

        android.view.animation.Animation outtoBottom = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f,
                android.view.animation.Animation.RELATIVE_TO_PARENT,  0.0f, android.view.animation.Animation.RELATIVE_TO_PARENT,   +1.0f
        );
        outtoBottom.setDuration(500);
        outtoBottom.setInterpolator(new AccelerateInterpolator());
        return outtoBottom;

    }
}
