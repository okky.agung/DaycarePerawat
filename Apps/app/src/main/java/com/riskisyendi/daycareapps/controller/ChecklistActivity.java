package com.riskisyendi.daycareapps.controller;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.riskisyendi.daycareapps.R;
import com.riskisyendi.daycareapps.adapter.ChecklistExpAdapter;
import com.riskisyendi.daycareapps.common.AnimatedExpandableListView;
import com.riskisyendi.daycareapps.rowItem.ChecklistChildRowItem;
import com.riskisyendi.daycareapps.rowItem.ChecklistGroupRowItem;

import java.util.ArrayList;
import java.util.HashMap;

public class ChecklistActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    ChecklistExpAdapter listAdapter;
    AnimatedExpandableListView expListView;
    //    ArrayList<String> headerList;
    private ArrayList<ChecklistGroupRowItem> headerList;
    HashMap<ChecklistGroupRowItem, ArrayList<ChecklistChildRowItem>> childList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist);

        toolbar();
        pageChecklist();

    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.checklist);
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.textWhite));
        mToolbar.setNavigationIcon(R.drawable.ic_chevron_left);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void pageChecklist(){
        // get the listview
        expListView = (AnimatedExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ChecklistExpAdapter(this, headerList, childList);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview ChecklistGroupRowItem click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // We call collapseGroupWithAnimation(int) and
                // expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (expListView.isGroupExpanded(groupPosition)) {
                    expListView.collapseGroupWithAnimation(groupPosition);
                    Log.d("MainActivity", "Collapsing");
                } else {
                    expListView.expandGroupWithAnimation(groupPosition);
                    Log.d("MainActivity", "Expanding");
                }
                return true;
            }

        });

        // Listview ChecklistGroupRowItem expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {


            @Override
            public void onGroupExpand(int groupPosition) {
                    /*Toast.makeText(getApplicationContext(),
                            headerList.get(groupPosition) + " Expanded",
                            Toast.LENGTH_SHORT).show();*/
            }

        });

        //Listview ChecklistGroupRowItem collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                    /*Toast.makeText(getApplicationContext(),
                            headerList.get(groupPosition) + " Collapsed",
                            Toast.LENGTH_SHORT).show();*/

            }
        });

        //Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getApplicationContext(),
                        headerList.get(groupPosition)
                                + " : "
                                + childList.get(
                                headerList.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });

    }

    private void prepareListData() {

        headerList = new ArrayList<ChecklistGroupRowItem>();
        childList = new HashMap<ChecklistGroupRowItem, ArrayList<ChecklistChildRowItem>>();

        // Adding child data

        headerList.add(new ChecklistGroupRowItem("Dapat merespon perilaku keagamaan secara sederhana"));
        headerList.add(new ChecklistGroupRowItem("Dapat mengekpresikan rasa sayang atau cinta kasih sesamanya"));
        headerList.add(new ChecklistGroupRowItem("Dapat meniru perilaku yang baik dan sopan"));
        headerList.add(new ChecklistGroupRowItem("Dapat berinteraksi dengan lingkungan terdekat"));
        headerList.add(new ChecklistGroupRowItem("Dapat menunjukkan keinginannya"));
        headerList.add(new ChecklistGroupRowItem("Dapat menunjukkan kemandirian"));

//        headerList.add("Top 250");
//        headerList.add("Now Showing");
//        headerList.add("Coming Soon..");

        // Adding child data
        ArrayList<ChecklistChildRowItem> religion1 = new ArrayList<ChecklistChildRowItem>();
        religion1.add(new ChecklistChildRowItem("1","Menyebut nama Tuhan."));
        religion1.add(new ChecklistChildRowItem("2","Mengikuti bacaan doa/berdoa sebelum dan sesudah melakukan kegiatan."));
        religion1.add(new ChecklistChildRowItem("3","Menyanyikan lagu keagamaan."));
        religion1.add(new ChecklistChildRowItem("4","Mengucapkan salam keagamaan."));

        ArrayList<ChecklistChildRowItem> religion2 = new ArrayList<ChecklistChildRowItem>();
        religion2.add(new ChecklistChildRowItem("1","Menunjukkan rasa sayang dan cinta kasih melalui belaian/ rangkulan."));
        religion2.add(new ChecklistChildRowItem("2","Menyayangi binatang."));
        religion2.add(new ChecklistChildRowItem("3","Memelihara tanaman."));
        religion2.add(new ChecklistChildRowItem("4","Suka menolong teman."));

        ArrayList<ChecklistChildRowItem> religion3 = new ArrayList<ChecklistChildRowItem>();
        religion3.add(new ChecklistChildRowItem("1","Mengucapkan salam, terima kasih, minta tolong secara sederhana."));
        religion3.add(new ChecklistChildRowItem("2","Mau menjawab sapaan dengan ramah."));

        ArrayList<ChecklistChildRowItem> social1 = new ArrayList<ChecklistChildRowItem>();
        social1.add(new ChecklistChildRowItem("1","Mulai menunjukkan senang bermain dengan teman."));
        social1.add(new ChecklistChildRowItem("2","Merespon terhadap beberapa nama teman bermain."));
        social1.add(new ChecklistChildRowItem("3","Senang meniru apa yang dilakukan orang lain."));
        social1.add(new ChecklistChildRowItem("4","Mau menyapa teman."));

        ArrayList<ChecklistChildRowItem> social2 = new ArrayList<ChecklistChildRowItem>();
        social2.add(new ChecklistChildRowItem("1","Mau memilih sesuatu yang disukai."));
        social2.add(new ChecklistChildRowItem("2","Mempertahankan hak milik."));
        social2.add(new ChecklistChildRowItem("3","Menunjuk benda miliknya."));

        ArrayList<ChecklistChildRowItem> social3 = new ArrayList<ChecklistChildRowItem>();
        social3.add(new ChecklistChildRowItem("1","Dapat ditinggalkan oleh orangtuanya."));
        social3.add(new ChecklistChildRowItem("2","Memilih kegiatan sendiri."));
        social3.add(new ChecklistChildRowItem("3","Mulai dapat menggunakan toilet (wc) namun masih dibantu atau diingatkan."));
        social3.add(new ChecklistChildRowItem("4","Makan dan minum sendiri."));

        childList.put(headerList.get(0), religion1); // Header, ChecklistChildRowItem data
        childList.put(headerList.get(1), religion2);
        childList.put(headerList.get(2), religion3);
        childList.put(headerList.get(3), social1);
        childList.put(headerList.get(4), social2);
        childList.put(headerList.get(5), social3);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
