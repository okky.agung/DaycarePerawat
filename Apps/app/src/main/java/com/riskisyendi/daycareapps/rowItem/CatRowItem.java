package com.riskisyendi.daycareapps.rowItem;

/**
 * Created by riskisyendi on 3/6/17.
 */

public class CatRowItem {
    Integer id, intBackgroundList;
    String strCat;

    public CatRowItem(Integer id, Integer intBackgroundList, String strCat) {
        this.id = id;
        this.intBackgroundList = intBackgroundList;
        this.strCat = strCat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIntBackgroundList() {
        return intBackgroundList;
    }

    public void setIntBackgroundList(Integer intBackgroundList) {
        this.intBackgroundList = intBackgroundList;
    }

    public String getStrCat() {
        return strCat;
    }

    public void setStrCat(String strCat) {
        this.strCat = strCat;
    }
}
