package com.riskisyendi.daycareapps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.riskisyendi.daycareapps.rowItem.ChecklistChildRowItem;
import com.riskisyendi.daycareapps.rowItem.ChecklistGroupRowItem;
import com.riskisyendi.daycareapps.R;
import com.riskisyendi.daycareapps.common.AnimatedExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by riskisyendi on 10/17/16.
 */

public class ChecklistExpAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {


//    com.appscapeblog.expandablelistview.AnimatedExpandableListView.AnimatedExpandableListAdapter
    private Context context;
    private ArrayList<ChecklistGroupRowItem> headerList; // header titles
    // child data in format of header title, child title
    private HashMap<ChecklistGroupRowItem, ArrayList<ChecklistChildRowItem>> childList;

    public ChecklistExpAdapter(Context context, ArrayList<ChecklistGroupRowItem> headerList,
                               HashMap<ChecklistGroupRowItem, ArrayList<ChecklistChildRowItem>> childList) {
        this.context = context;
        this.headerList = headerList;
        this.childList = childList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.childList.get(this.headerList.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getRealChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

        final ChecklistChildRowItem childText = (ChecklistChildRowItem) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_child_checklist, null);
        }

        TextView txtUniversity = (TextView) convertView.findViewById(R.id.txtNo);
        TextView txtFaculty = (TextView) convertView.findViewById(R.id.txtDesc);

        txtUniversity.setText(childText.getStrNo());
        txtFaculty.setText(childText.getStrDesc());



//        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return this.childList.get(this.headerList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headerList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.headerList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ChecklistGroupRowItem checklistGroupRowItemText = (ChecklistGroupRowItem) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_group_checklist, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);

        if (isExpanded){
            imgIcon.setImageResource(R.drawable.ic_arrow_drop_up);

        }else {

            imgIcon.setImageResource(R.drawable.ic_arrow_drop_down);
        }

        TextView txtHeaderName = (TextView) convertView.findViewById(R.id.header_name);

        txtHeaderName.setText(checklistGroupRowItemText.getStrHeaderName());
//        TextView listHeader = (TextView) convertView.findViewById(R.id.header_list);
//        listHeader.setTypeface(null, Typeface.BOLD);
//        listHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}


//public class ChecklistExpAdapter extends com.indonesia.digivla.expandablelistviewcustom3.AnimatedExpandableListView.AnimatedExpandableListAdapter {
//
//
//    //    com.appscapeblog.expandablelistview.AnimatedExpandableListView.AnimatedExpandableListAdapter
//    private Context context;
//    private ArrayList<String> headerList; // header titles
//    // child data in format of header title, child title
//    private HashMap<String, List<String>> childList;
//
//    public ChecklistExpAdapter(Context context, ArrayList<String> headerList,
//                             HashMap<String, List<String>> childList) {
//        this.context = context;
//        this.headerList = headerList;
//        this.childList = childList;
//    }
//
//    @Override
//    public Object getChild(int groupPosition, int childPosititon) {
//        return this.childList.get(this.headerList.get(groupPosition)).get(childPosititon);
//    }
//
//    @Override
//    public long getChildId(int groupPosition, int childPosition) {
//        return childPosition;
//    }
//
//    @Override
//    public View getRealChildView(int groupPosition, final int childPosition,
//                                 boolean isLastChild, View convertView, ViewGroup parent) {
//
//        final String childText = (String) getChild(groupPosition, childPosition);
//
//        if (convertView == null) {
//            LayoutInflater infalInflater = (LayoutInflater) this.context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = infalInflater.inflate(R.layout.list_item_child_checklist, null);
//        }
//
//        TextView txtListChild = (TextView) convertView.findViewById(R.id.child_list);
//        txtListChild.setText(childText);
//        return convertView;
//    }
//
//    @Override
//    public int getRealChildrenCount(int groupPosition) {
//        return this.childList.get(this.headerList.get(groupPosition)).size();
//    }
//
//    @Override
//    public Object getGroup(int groupPosition) {
//        return this.headerList.get(groupPosition);
//    }
//
//    @Override
//    public int getGroupCount() {
//        return this.headerList.size();
//    }
//
//    @Override
//    public long getGroupId(int groupPosition) {
//        return groupPosition;
//    }
//
//    @Override
//    public View getGroupView(int groupPosition, boolean isExpanded,
//                             View convertView, ViewGroup parent) {
//        String headerTitle = (String) getGroup(groupPosition);
//        if (convertView == null) {
//            LayoutInflater infalInflater = (LayoutInflater) this.context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = infalInflater.inflate(R.layout.list_item_group_checklist, null);
//        }
//
//        TextView listHeader = (TextView) convertView.findViewById(R.id.header_list);
//        listHeader.setTypeface(null, Typeface.BOLD);
//        listHeader.setText(headerTitle);
//
//        return convertView;
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }
//
//    @Override
//    public boolean isChildSelectable(int groupPosition, int childPosition) {
//        return true;
//    }
//}