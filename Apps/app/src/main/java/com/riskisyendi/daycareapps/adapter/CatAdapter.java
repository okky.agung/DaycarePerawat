package com.riskisyendi.daycareapps.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.riskisyendi.daycareapps.R;
import com.riskisyendi.daycareapps.rowItem.CatRowItem;

import java.util.ArrayList;

/**
 * Created by riskisyendi on 3/6/17.
 */

public class CatAdapter extends ArrayAdapter<CatRowItem> {

    Context context;
    int layoutResourceId;
    //    String id;
    ArrayList<CatRowItem> mCatArrayList = new ArrayList<CatRowItem>();

    public CatAdapter(Context context, int layoutResourceId, ArrayList<CatRowItem> mCatArrayList) {
        super(context, layoutResourceId, mCatArrayList);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.mCatArrayList = mCatArrayList;
    }

    static class UserHolder {
        ImageView mImgCat;
        RelativeLayout mRltBackgroundList;
        TextView mTxtCat;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new UserHolder();
//            holder.mImgCat = (ImageView) row.findViewById(R.id.imgCat);
            holder.mRltBackgroundList = (RelativeLayout) row.findViewById(R.id.rltBackgroundList);
            holder.mTxtCat = (TextView) row.findViewById(R.id.txtCat);
            row.setTag(holder);

        } else {
            holder = (UserHolder) row.getTag();
        }

        CatRowItem mCatRowItem = mCatArrayList.get(position);
//        holder.mImgCat.setImageResource(mCatRowItem.getIntBackgroundList());
        holder.mRltBackgroundList.setBackgroundResource(mCatRowItem.getIntBackgroundList());
        holder.mTxtCat.setText(mCatRowItem.getStrCat());

        return row;

    }

}
