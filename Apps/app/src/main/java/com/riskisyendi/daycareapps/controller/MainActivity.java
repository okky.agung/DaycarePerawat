package com.riskisyendi.daycareapps.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.riskisyendi.daycareapps.R;
import com.riskisyendi.daycareapps.adapter.CatAdapter;
import com.riskisyendi.daycareapps.rowItem.CatRowItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private ListView mListCat;
    private CatAdapter mCatAdapter;
    private ArrayList<CatRowItem> mCatArrayList = new ArrayList<CatRowItem>();
    private ImageView mImgCat;
    private RelativeLayout mRltBackgroundList;
    private TextView mTxtCat;
    private String strCatID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar();
        navBar();
        pageMain();
    }

    public void toolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    public void navBar(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void pageMain(){

//        mImgCat = (ImageView) findViewById(R.id.imgCat);
        mRltBackgroundList = (RelativeLayout) findViewById(R.id.rltBackgroundList);
        mTxtCat = (TextView) findViewById(R.id.txtCat);

        mCatArrayList.add(new CatRowItem(401,R.drawable.pray,"MORAL DAN NILAI AGAMA"));
        mCatArrayList.add(new CatRowItem(402,R.drawable.social,"SOSIAL, EMOSIONAL DAN KEMANDIRIAN"));
        mCatArrayList.add(new CatRowItem(403,R.drawable.language,"BAHASA"));
        mCatArrayList.add(new CatRowItem(403,R.drawable.cognitif,"KOGNITIF"));
        mCatArrayList.add(new CatRowItem(403,R.drawable.motoric,"FISIK/MOTORIK"));
        mCatArrayList.add(new CatRowItem(403,R.drawable.art,"SENI"));

        /**
         * set item into adapter
         */
        mCatAdapter = new CatAdapter(this, R.layout.list_item_cat, mCatArrayList);
        mListCat = (ListView) findViewById(R.id.listCat);
        mListCat.setItemsCanFocus(false);
        mListCat.setAdapter(mCatAdapter);

        /**
         * get on item click listener
         */

        mListCat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, final int position, long id) {
//                Toast.makeText(getActivity(), , Toast.LENGTH_LONG).show();
                CatRowItem directoryListRowItem = (CatRowItem) parent.getItemAtPosition(position);
//                Toast.makeText(getApplication(),
//                        "Clicked on Row: " + directoryListRowItem.getListDirectory() + position,
//                        Toast.LENGTH_LONG).show();
                // Sending image id to FullScreenActivity
//                strDirListID = directoryListRowItem.getId().toString();
                Intent i = new Intent(MainActivity.this, ChecklistActivity.class);
                // passing array index
//                i.putExtra("exDirListID", strDirListID);
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.btnProfile) {
            Toast.makeText(this, "Menu Profile dalam pengembangan",
                    Toast.LENGTH_LONG).show();
        } else if (id == R.id.btnSetting) {
            Toast.makeText(this, "Menu Setting dalam pengembangan",
                    Toast.LENGTH_LONG).show();

        } else if (id == R.id.btnLogout) {

            Intent i = new Intent(MainActivity.this, AccountActivity.class);
            startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
